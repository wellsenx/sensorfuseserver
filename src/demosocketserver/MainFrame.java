/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demosocketserver;

import java.awt.Dimension;
import java.awt.List;
import java.awt.Rectangle;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Properties;
import javax.swing.JOptionPane;
import javax.swing.JScrollBar;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONWriter;

/**
 *
 * @author user
 */
public class MainFrame extends javax.swing.JFrame {

    public static final String PROPERTY_FILENAME = "socket_server.props";
    public static final String KEY_POS_X = "posX";
    public static final String KEY_POS_Y = "posY";
    public static final String KEY_LISTENING_PORT_LIVE = "listeningPortLive";
    public static final String KEY_LISTENING_PORT_FILE = "listeningPortFile";
    public static final String KEY_LISTENING_PORT_FILE_VIDEO = "listeningPortFileVideo";
    
    private int listeningPortLive = 5000;
    private int listeningPortFile = 5001;
    private int listeningPortFileVideo = 5002;
    
    private ThreadServerSocketLive thServerSocketLive;
    private ThreadServerSocketFile thServerSocketFile;
    private ThreadServerSocketFile thServerSocketFileVideo;
    
    private String filenameLiveCSV = "live_value.csv";
    private String filenameLiveJSON = "live_value.json";
    
    /**
     * Creates new form MainFrame
     */
    public MainFrame() {
        initComponents();
        
        doInit();
        loadAppProperties();
    }

    private void doInit() {
        lblStatus.setText("Status: Stopped");
        btnStartStop.setText("Start");
        
        this.setSize(640, 480);
    }
    
    private void loadAppProperties() {
        Dimension screenSize = this.getToolkit().getScreenSize();
        int posXDefault = (screenSize.width - this.getWidth()) / 2;
        int posYDefault = (screenSize.height - this.getHeight()) / 2;

        Properties thisProperties = new Properties();
        File fileProperties = new File(PROPERTY_FILENAME);

        try {
            if (!fileProperties.exists()) {
                try {
                    fileProperties.createNewFile();
                } catch (IOException ioe) {
                    JOptionPane.showMessageDialog(null, "Cannot create property file!", "Warning", JOptionPane.WARNING_MESSAGE);
                }
            }
            
            FileInputStream fInput = new FileInputStream(fileProperties);
            thisProperties.load(fInput);
            fInput.close();
        } catch (IOException ioe) {
            JOptionPane.showMessageDialog(null, "Cannot read the property file!", "Warning", JOptionPane.WARNING_MESSAGE);
        }

        int posX = Integer.parseInt(thisProperties.getProperty(KEY_POS_X, Integer.toString(posXDefault)));
        int posY = Integer.parseInt(thisProperties.getProperty(KEY_POS_Y, Integer.toString(posYDefault)));
        listeningPortLive = Integer.parseInt(thisProperties.getProperty(KEY_LISTENING_PORT_LIVE, Integer.toString(listeningPortLive)));
        listeningPortFile = Integer.parseInt(thisProperties.getProperty(KEY_LISTENING_PORT_FILE, Integer.toString(listeningPortFile)));
        listeningPortFileVideo = Integer.parseInt(thisProperties.getProperty(KEY_LISTENING_PORT_FILE_VIDEO, Integer.toString(listeningPortFileVideo)));
        
        this.setLocation(posX, posY);
    }
    
    private class ThreadServerSocketLive extends Thread {
    
        private ServerSocket serverSocketLive;
        private ArrayList<ThreadReadSocketLive> list;
        
        private boolean isRun = false;
        
        public ThreadServerSocketLive() {
            
        }
        
        public void run() {
            try {
                serverSocketLive = new ServerSocket(listeningPortLive);
                list = new ArrayList<ThreadReadSocketLive>();
                isRun = true;                
             
                if (serverSocketLive != null) {
                    while (isRun) {
                        Socket socket = serverSocketLive.accept();
                        
                        ThreadReadSocketLive thReadSocketLive = new ThreadReadSocketLive(socket);
                        list.add(thReadSocketLive);
                        thReadSocketLive.start();
                        
                        System.out.println("client accepted");
                    }                    
                }
            } catch (SocketException e) {
                //e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                stopSelf();
            }
            
            System.out.println("Stopped");
        }
        
        public void stopThread() { 
            isRun = false;
            
            stopSelf();
        }
        
        private void stopSelf() {
            for (int i = 0; i < list.size(); i++) {
                list.get(i).stopThread();
            }
            
            if (serverSocketLive != null) {
                try {
                    serverSocketLive.close();
                    serverSocketLive = null;
                } catch (IOException ioe) {
                }

                System.out.println("Socket closed");
            }
        }
        
        public boolean isRun() {
            return isRun;
        }
    }
    
    private class ThreadReadSocketLive extends Thread {
        
        private boolean isRun = false;
        private Socket socket = null;
        private BufferedReader reader = null;
        private BufferedOutputStream output = null;
        
        private BufferedInputStream readerJSON = null;
        private BufferedOutputStream outputJSON = null;
        
        public ThreadReadSocketLive(Socket socket) {
            this.socket = socket;
        }
        
        public void run() {
            isRun = true;                    
     
            try {
                output = new BufferedOutputStream(new FileOutputStream(filenameLiveCSV, true));
                reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                
                String str = null;
                while (isRun) {
                    try {
                        if (reader.ready()) {
                            str = reader.readLine();
                            
                            if (str != null && str.length() > 0) {
                                output.write((str + "\n").getBytes());
                                output.flush();
                            
                                txtLog.setText(txtLog.getText() + str + "\n");
                                
                                JScrollBar vertical = scrLog.getVerticalScrollBar();
                                vertical.setValue(vertical.getMaximum());
                                
                                //--
                                JSONObject object = null;
                                
                                String[] strings = str.split(",");
                                if (strings[0].equalsIgnoreCase(SensorConstants.KEY_SENSOR_TYPE_LOCATION)) {
                                    try {                
                                        object = new JSONObject();
                                              object.append(SensorConstants.KEY_SENSOR_TYPE, strings[0]);
                                              object.append(SensorConstants.KEY_TIMESTAMP, strings[1]);
                                              object.append(SensorConstants.KEY_DATE, strings[2]);
                                              object.append(SensorConstants.KEY_LATITUDE, strings[3]);
                                              object.append(SensorConstants.KEY_LONGITUDE, strings[4]);
                                    } catch (Exception e) {
                                      e.printStackTrace();
                                    }
                                  } else if (strings[0].equalsIgnoreCase(SensorConstants.KEY_SENSOR_TYPE_ACCELEROMETER) ||
                                      strings[0].equalsIgnoreCase(SensorConstants.KEY_SENSOR_TYPE_GYROSCOPE) ||
                                      strings[0].equalsIgnoreCase(SensorConstants.KEY_SENSOR_TYPE_LINEAR_ACCELERATION) ||
                                      strings[0].equalsIgnoreCase(SensorConstants.KEY_SENSOR_TYPE_GRAVITY)) {

                                    try {
                                        object = new JSONObject();
                                                  object.append(SensorConstants.KEY_SENSOR_TYPE, strings[0]);
                                                  object.append(SensorConstants.KEY_TIMESTAMP, strings[1]);
                                                  object.append(SensorConstants.KEY_DATE, strings[2]);
                                                  object.append(SensorConstants.KEY_X, strings[3]);
                                                  object.append(SensorConstants.KEY_Y, strings[4]);
                                                  object.append(SensorConstants.KEY_Z, strings[5]);
                                                  
                                    } catch (Exception e) {
                                      e.printStackTrace();
                                    }

                                  } else if (strings[0].equalsIgnoreCase(SensorConstants.KEY_SENSOR_TYPE_PROXIMITY)) {
                                    try {
                                        object = new JSONObject();
                                                  object.append(SensorConstants.KEY_SENSOR_TYPE, strings[0]);
                                                  object.append(SensorConstants.KEY_TIMESTAMP, strings[1]);
                                                  object.append(SensorConstants.KEY_DATE, strings[2]);
                                                  object.append(SensorConstants.KEY_DISTANCE, strings[3]);
                                                  
                                    } catch (Exception e) {
                                      e.printStackTrace();
                                    }
                                  } else if (strings[0].equalsIgnoreCase(SensorConstants.KEY_SENSOR_TYPE_ORIENTATION)) {
                                    try {
                                       object = new JSONObject();
                                                  object.append(SensorConstants.KEY_SENSOR_TYPE, strings[0]);
                                                  object.append(SensorConstants.KEY_TIMESTAMP, strings[1]);
                                                  object.append(SensorConstants.KEY_DATE, strings[2]);
                                                  object.append(SensorConstants.KEY_AZIMUT, strings[3]);
                                                  object.append(SensorConstants.KEY_PITCH, strings[4]);
                                                  object.append(SensorConstants.KEY_ROLL, strings[5]);
                                                  
                                    } catch (Exception e) {
                                      e.printStackTrace();
                                    }
                                  } else if (strings[0].equalsIgnoreCase(SensorConstants.KEY_SENSOR_TYPE_BAROMETER)) {
                                    try {
                                        object = new JSONObject();
                                                  object.append(SensorConstants.KEY_SENSOR_TYPE, strings[0]);
                                                  object.append(SensorConstants.KEY_TIMESTAMP, strings[1]);
                                                  object.append(SensorConstants.KEY_DATE, strings[2]);
                                                  object.append(SensorConstants.KEY_PRESSURE, strings[3]);
                                                  
                                    } catch (Exception e) {
                                      e.printStackTrace();
                                    }
                                  } else if (strings[0].equalsIgnoreCase(SensorConstants.KEY_SENSOR_TYPE_THERMOMETER)) {
                                    try {
                                        object = new JSONObject();
                                                  object.append(SensorConstants.KEY_SENSOR_TYPE, strings[0]);
                                                  object.append(SensorConstants.KEY_TIMESTAMP, strings[1]);
                                                  object.append(SensorConstants.KEY_DATE, strings[2]);
                                                  object.append(SensorConstants.KEY_TEMPERATURE, strings[3]);
                                                  
                                    } catch (Exception e) {
                                      e.printStackTrace();
                                    }
                                  }

                                if (object != null) {
                                    String sContent = "";
                                    try {
                                        readerJSON = new BufferedInputStream(new FileInputStream(filenameLiveJSON));    
                                        ByteArrayOutputStream outputBytes = new ByteArrayOutputStream();
                                    
                                        byte[] bytes = new byte[1024];
                                        int count = 0;
                                        while ((count = readerJSON.read(bytes)) > 0) {
                                            outputBytes.write(bytes, 0, count);
                                            outputBytes.flush();
                                        }

                                        sContent= new String(outputBytes.toByteArray());
                                    } catch (Exception e) {
                                        //e.printStackTrace();
                                    } finally {
                                        if (readerJSON != null) {
                                            try {
                                                readerJSON.close();
                                            } catch (Exception e1) {
                                                
                                            }
                                        }
                                    }
                                    
                                    String stringToWrite = "";
                                    if (sContent.length() > 0) {
                                        stringToWrite = object.toString();
                                    } else {
                                        stringToWrite = ", " + object.toString(); 
                                    }
                                    
                                    outputJSON = new BufferedOutputStream(new FileOutputStream(filenameLiveJSON, true));
                                    outputJSON.write(stringToWrite.getBytes());
                                    outputJSON.flush();
                                    outputJSON.close();
                                }
                            }
                            
                            str = "";
                        }

                        //Thread.currentThread().sleep(100);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                stopSelf();
            }
        }
        
        public void stopThread() { 
            isRun = false;
            
            stopSelf();
        }
        
        private void stopSelf() {
            System.out.println("stopSelf");

            if (socket != null) {
                BufferedOutputStream outSocket = null;
                
                try {
                    outSocket = new BufferedOutputStream(socket.getOutputStream());
                    outSocket.write(("CLOSE_SOCKET\n").getBytes());
                    outSocket.flush();
                    System.out.println("CLOSE_SOCKET");
                } catch (IOException ioe) {
                } finally {
                    if (outSocket != null) {
                        try {
                            outSocket.close();
                        } catch (IOException e) {
                            
                        }
                    }
                    
                    try {
                        socket.close();
                    } catch (IOException e) {
                    }
                }
            }
                
            if (readerJSON != null) {
                try {
                    readerJSON.close();
                } catch (IOException ioe) {                     
                }
            }

            if (outputJSON != null) {
                try {
                    outputJSON.close();
                } catch (IOException ioe) {
                }
            }
            
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ioe) {                     
                }
            }

            if (output != null) {
                try {
                    output.close();
                } catch (IOException ioe) {
                }
            }
            
            if (socket != null) {   
                try {
                    socket.close();
                } catch (IOException e) {
                }
            }
        }
    }
    
    private class ThreadServerSocketFile extends Thread {
    
        private ServerSocket serverSocketFile;
        private ArrayList<ThreadReadSocketFile> list;
        private int listeningPortFile;
        private String type;
   
        private boolean isRun = false;     
        
        public ThreadServerSocketFile(int listeningPortFile, String type) {
            this.listeningPortFile = listeningPortFile;
            this.type = type;
        }
        
        public void run() {
            try {
                serverSocketFile = new ServerSocket(listeningPortFile);
                list = new ArrayList<ThreadReadSocketFile>();
                isRun = true;                
             
                if (serverSocketFile != null) {
                    while (isRun) {
                        Socket socket = serverSocketFile.accept();
                        
                        String filenameFile = "file_value_" + Calendar.getInstance().getTimeInMillis();
                        
                        if (type.equalsIgnoreCase("value")) {
                            filenameFile = filenameFile + ".json";
                        } else if (type.equalsIgnoreCase("video")) {
                            filenameFile = filenameFile + ".3gp";
                        }
                        
                        ThreadReadSocketFile thReadSocketFile = new ThreadReadSocketFile(socket, filenameFile);
                        list.add(thReadSocketFile);
                        thReadSocketFile.start();
                        
                        System.out.println("client accepted");
                    }                    
                }
            } catch (SocketException e) {
                //e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                stopSelf();
            }
            
            System.out.println("Stopped");
        }
        
        public void stopThread() { 
            isRun = false;
            
            stopSelf();
        }
        
        private void stopSelf() {
            if (serverSocketFile != null) {
                try {
                    serverSocketFile.close();
                    serverSocketFile = null;
                } catch (IOException ioe) {

                }

                System.out.println("Socket closed");
            }
        }
        
        public boolean isRun() {
            return isRun;
        }
    }
    
    private class ThreadReadSocketFile extends Thread {
        
        private Socket socket = null;
        private BufferedInputStream reader = null;
        private BufferedOutputStream output = null;
        private String filenameFile = "";
        
        public ThreadReadSocketFile(Socket socket, String filenameFile) {
            this.socket = socket;
            this.filenameFile = filenameFile;
        }
        
        public void run() {
            try {
                output = new BufferedOutputStream(new FileOutputStream(filenameFile));
                reader = new BufferedInputStream((socket.getInputStream()));
                byte[] bytes = new byte[1024];
                int count = -1;
            
                while ((count = reader.read(bytes)) != -1) {
                    output.write(bytes, 0, count);
                    output.flush();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException ioe) {                     
                    }
                }
                
                if (output != null) {
                    try {
                        output.close();
                    } catch (IOException ioe) {
                    }
                }
                
                if (socket != null) {
                    try {
                        socket.close();
                    } catch (IOException ioe) {
                    }
                }
            }
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblStatus = new javax.swing.JLabel();
        btnStartStop = new javax.swing.JButton();
        scrLog = new javax.swing.JScrollPane();
        txtLog = new javax.swing.JTextArea();
        btnClear = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        mnuApp = new javax.swing.JMenu();
        mnuItemSettings = new javax.swing.JMenuItem();
        mnuHelp = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lblStatus.setText("Status");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        btnStartStop.setText("START");
        btnStartStop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStartStopActionPerformed(evt);
            }
        });

        txtLog.setEditable(false);
        txtLog.setColumns(20);
        txtLog.setRows(5);
        scrLog.setViewportView(txtLog);

        btnClear.setText("CLEAR");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        mnuApp.setText("App");

        mnuItemSettings.setText("Settings");
        mnuItemSettings.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuItemSettingsActionPerformed(evt);
            }
        });
        mnuApp.add(mnuItemSettings);

        jMenuBar1.add(mnuApp);

        mnuHelp.setText("Help");
        jMenuBar1.add(mnuHelp);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scrLog, javax.swing.GroupLayout.DEFAULT_SIZE, 351, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnStartStop))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnClear)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnStartStop))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(scrLog, javax.swing.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnClear)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnStartStopActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStartStopActionPerformed
        // TODO add your handling code here:
        if ((thServerSocketLive == null || !thServerSocketLive.isRun()) /*&&
                (thServerSocketFile == null || !thServerSocketFile.isRun())*/) {
            lblStatus.setText("Status: Started");
            btnStartStop.setText("Stop");
            mnuItemSettings.setEnabled(false);
            
            //thServerSocketLive = new ThreadServerSocketLive();
            //thServerSocketLive.start();
            
            thServerSocketFile = new ThreadServerSocketFile(listeningPortFile, "value");
            thServerSocketFile.start();
            
            thServerSocketFileVideo = new ThreadServerSocketFile(listeningPortFileVideo, "video");
            thServerSocketFileVideo.start();
            
            System.out.println("start");
        } else {
            lblStatus.setText("Status: Stopped");
            btnStartStop.setText("Start");
            
            //thServerSocketLive.stopThread();
            thServerSocketFile.stopThread();
            thServerSocketFileVideo.stopThread();
            
            mnuItemSettings.setEnabled(true);
        }
    }//GEN-LAST:event_btnStartStopActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
        btnStartStopActionPerformed(null);
        
        Properties thisProperties = new Properties();
        thisProperties.setProperty(KEY_POS_X, Integer.toString(this.getX()));
        thisProperties.setProperty(KEY_POS_Y, Integer.toString(this.getY()));
        thisProperties.setProperty(KEY_LISTENING_PORT_LIVE, Integer.toString(listeningPortLive));
        thisProperties.setProperty(KEY_LISTENING_PORT_FILE, Integer.toString(listeningPortFile));
        thisProperties.setProperty(KEY_LISTENING_PORT_FILE_VIDEO, Integer.toString(listeningPortFileVideo));

        File fileProperties = new File(PROPERTY_FILENAME);
        try {
            if (!fileProperties.exists()) {
                try {
                    fileProperties.createNewFile();
                } catch (IOException ioe) {
                    JOptionPane.showMessageDialog(this, "Cannot create property file!", "Warning", JOptionPane.WARNING_MESSAGE);
                }
            }
            
            FileOutputStream fOutput = new FileOutputStream(fileProperties);
            thisProperties.store(fOutput, "Property values");
            fOutput.close();
        } catch (IOException ioe) {
            JOptionPane.showMessageDialog(this, "Cannot save property values!", "Warning", JOptionPane.WARNING_MESSAGE);
        }
        // </editor-fold>
        
        this.setVisible(false);
        this.dispose();
        
        System.exit(0);
    }//GEN-LAST:event_formWindowClosing

    private void mnuItemSettingsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuItemSettingsActionPerformed
        // TODO add your handling code here:
        DialogSettings dialogSettings = new DialogSettings(this, true, listeningPortLive, listeningPortFile, listeningPortFileVideo);
        dialogSettings.setVisible(true);
        
        loadAppProperties();
    }//GEN-LAST:event_mnuItemSettingsActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        // TODO add your handling code here:
        txtLog.setText("");
    }//GEN-LAST:event_btnClearActionPerformed

    // <editor-fold desc="ui fields">
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnStartStop;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblStatus;
    private javax.swing.JMenu mnuApp;
    private javax.swing.JMenu mnuHelp;
    private javax.swing.JMenuItem mnuItemSettings;
    private javax.swing.JScrollPane scrLog;
    private javax.swing.JTextArea txtLog;
    // End of variables declaration//GEN-END:variables
    // </editor-fold>
}
